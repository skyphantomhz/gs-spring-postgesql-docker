package com.judgesystem.core;

import java.io.*;

/**
 * Created by mastermind on 3/24/17.
 */
public class processor {
    String code="#include<iostream>\n" +
            "using namespace std;\n" +
            "int main(){\n" +
            "\tint a,b;\n" +
            "\tstring c;\n" +
            "\tcin>>a;\n" +
            "\tcin>>b;\n" +
            "\tcin>>c;\n" +
            "\tcout<<a+b<<\"  \"<<c;\n" +
            "\treturn 0;\n" +
            "}\n";
    public processor()  {
        try {

            Comparator.inputFile(code, "src/main/resources/code/exec.cpp");
            compilerCode("g++ -O2 -s -Wall -std=c++11 -o src/main/resources/code/exec src/main/resources/code/exec.cpp -lm");
        }catch (IOException e){
            e.getStackTrace();
        }
    }

    public void compilerCode(String commandCompiler){
        try {
            String line;
            Process p = Runtime.getRuntime().exec(commandCompiler);
            BufferedReader bri = new BufferedReader
                    (new InputStreamReader(p.getInputStream()));
            BufferedReader bre = new BufferedReader
                    (new InputStreamReader(p.getErrorStream()));
            Comparator.inputFile(bre.toString(),"src/main/resources/code/execLoi.txt");
            System.out.println("gi day?");
            while ((line = bri.readLine()) != null) {
                System.out.println(line);
            }
            bri.close();
            System.out.println("day la gi?");
            while ((line = bre.readLine()) != null) {
                System.out.println(line);
            }
            bre.close();
            p.waitFor();
            System.out.println("Done.");
        }
        catch (Exception err) {
            err.printStackTrace();
        }
    }
    public void runCode(String commandRun) throws IOException{
        String line;
        OutputStream stdin = null;
        InputStream stderr = null;
        InputStream stdout = null;

        // launch EXE and grab stdin/stdout and stderr
        Process process = Runtime.getRuntime ().exec ("src/main/resources/code/exec");
        stdin = process.getOutputStream ();
        stderr = process.getErrorStream ();
        stdout = process.getInputStream ();

        // "write" the parms into stdin
        line = "2 2 as" + "\n";
        stdin.write(line.getBytes() );
        stdin.flush();


        stdin.close();
        System.out.println("Hello");
        // clean up if any output in stdout
        BufferedReader brCleanUp =
                new BufferedReader (new InputStreamReader (stdout));
        Comparator.inputFile(brCleanUp.toString(),"src/main/resources/code/execOutput.txt");
        while ((line = brCleanUp.readLine ()) != null) {
            System.out.println ("[Stdout] " + line);
        }
        brCleanUp.close();

        // clean up if any output in stderr
        brCleanUp =
                new BufferedReader (new InputStreamReader (stderr));
        while ((line = brCleanUp.readLine ()) != null) {
            System.out.println ("[Stderr] " + line);
        }
        brCleanUp.close();
    }
}
