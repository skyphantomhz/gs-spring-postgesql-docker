package com.judgesystem.repository;

import org.springframework.data.repository.CrudRepository;

import com.judgesystem.model.bean.Userend;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<Userend, Long> {

}
